package main

import (
	"fmt"
	"time"

	vegeta "github.com/tsenart/vegeta/lib"
)

func main() {
	targets, _ := vegeta.NewTargets([]string{"GET http://localhost:8090/cookies"})
	rate := uint64(100) // per second
	duration := 4 * time.Second

	results := vegeta.Attack(targets, rate, duration)
	metrics := vegeta.NewMetrics(results)

	fmt.Printf("Mean latency: %s", metrics.Latencies.Mean)
}
