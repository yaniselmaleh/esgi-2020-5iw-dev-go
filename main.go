package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Cookie struct {
	CookieCounter int    `json:"CookieCounter"`
	Name          string `json:"Name"`
}

type Page struct {
	Title   string
	Cookies []string
}

var Cookies []Cookie

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Headers", "Content-Type")
}

func accueil(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Accueil")
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprintf(w, "<h1>Accueil</h1>")
	fmt.Fprintf(w, "<a href='play'>Commencer le jeu</a>")
}

func handleRequests() {
	Router := mux.NewRouter()
	Router.HandleFunc("/", accueil)
	Router.HandleFunc("/play", viewTemplate)
	Router.HandleFunc("/cookies", returnCookies).Methods("GET")
	Router.HandleFunc("/cookie", createCookie).Methods("POST")
	log.Fatal(http.ListenAndServe(":8090", Router))

	// fs := http.FileServer(http.Dir("./public"))
	// Router.PathPrefix("/js/").Handler(fs)
	// Router.PathPrefix("/css/").Handler(fs)
}

func returnCookies(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	fmt.Println("Return Cookies")
	json.NewEncoder(w).Encode(Cookies)
	fmt.Println(Cookies)
}

func createCookie(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	fmt.Println("Create Cookie")
	reqBody, _ := ioutil.ReadAll(r.Body)

	var cookie Cookie
	json.Unmarshal(reqBody, &cookie)
	Cookies = append(Cookies, cookie)
	json.NewEncoder(w).Encode(cookie)
}

func viewTemplate(w http.ResponseWriter, r *http.Request) {
	p := Page{"Cookie Clikers !", []string{"Play cookie clickers"}}
	t := template.New("Main template")
	t = template.Must(t.ParseFiles("tmpl/layout.tmpl", "tmpl/content.tmpl"))
	err := t.ExecuteTemplate(w, "layout", p)

	if err != nil {
		log.Fatalf("Template execution: %s", err)
	}
}

func main() {
	Cookies = []Cookie{
		Cookie{CookieCounter: 1, Name: "Initial Player"},
	}
	handleRequests()
}
