FROM golang:1.14
WORKDIR /

COPY . .
RUN go get -d github.com/gorilla/mux
RUN go get -d github.com/tsenart/vegeta
RUN go get -d github.com/tsenart/vegeta/lib

CMD ["go","run","main.go"]