go:
	docker-compose up --build

goBuild:
	docker-compose run --rm app go build

build:
	docker-compose build

runGo:
	docker-compose run --rm app go run main.go

stop:
	docker-compose down

test1:
	ab -n 1000 -c 50 -T application/x-www-form-urlencoded http://localhost:8090/cookies

test2:
	ab -n 10000 -c 100 -T application/x-www-form-urlencoded -p data.json http://localhost:8090/cookie

vegeta1:
	echo "GET http://localhost:8090/cookies" | vegeta attack -name=50qps -rate=50 -duration=10s > results.bin cat results.bin | vegeta plot > result.vegeta.html

vegeta2:
	echo "GET http://localhost:8090/cookies" | vegeta attack -rate=100/s | vegeta encode > results.json
